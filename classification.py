from numpy import array, linspace
from sklearn.neighbors.kde import KernelDensity
from sklearn.grid_search import GridSearchCV
from scipy.signal import argrelextrema
from sklearn.decomposition import PCA
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import scipy, json, copy, sqlite3

DB_FILE = 'data.db'

"""
Criteria of Clustering
1) Constant : Have more than 70% of 25 parameter
2) Group : Have more than 30% of 25 parameter
3) Random : Otherwise
* Parameters which aret classified as Constant or Group are tied seperatly
** Col_parameters which are not same dimension are classified as Random
"""

"""int, double cls"""
def cls(values,s,mi,equipment_id,parameter_id):
    index_group, group_data = [], []

    for i in range(0,len(mi)+1):
        index_group.append([])

    if len(mi) == 0:
        if len(values) > 17:
            tmp = {}
            for i,value in enumerate(values):
                tmp.update({equipment_id[i]:value[0].item()})
            group_data.append(json.dumps(tmp))
            return ["Constant", group_data]
        elif len(values) > 7:
            tmp = {}
            for i,value in enumerate(values):
                tmp.update({equipment_id[i]:value[0].item()})
            group_data.append(json.dumps(tmp))
            return ["Group", group_data]
        else:
            for i, value in enumerate(values):
                group_data.append(json.dumps({equipment_id[i]:value[0].item()}))
            return ["Random", group_data]
    elif len(mi) == 1:
        for i, value in enumerate(values):
            if value < s[mi[0]]:
                index_group[0].append(i)
            else:
                index_group[1].append(i)
    else:
        for i, value in enumerate(values):
            for j in range(0,len(mi)):
                if j ==0 :
                    if value < s[mi[j]]:
                        index_group[j].append(i)
                        break
                elif j == len(mi) -1:
                    if value > s[mi[j-1]] and value < s[mi[j]]:
                        index_group[j].append(i)
                        break
                    elif value > s[mi[j]]:
                        index_group[j+1].append(i)
                        break
                else:
                    if value > s[mi[j-1]] and value < s[mi[j]]:
                        index_group[j].append(i)
                        break
    print("index_group : ",index_group)
    cnt = 0
    for i, indexes in enumerate(index_group):
        if len(indexes) > 7:
            tmp = {}
            if len(indexes) > cnt:
                cnt = len(indexes)
            for j in  indexes:
                tmp.update({equipment_id[j]:values[j][0].item()})
            group_data.append(json.dumps(tmp))
        else:
            if len(indexes) > cnt:
                cnt = len(indexes)
            for j in indexes:
                group_data.append(json.dumps({equipment_id[j]:values[j][0].item()}))
    print("cnt : ",cnt)

    if cnt > 17:
        return ["Constant", group_data]
    elif cnt > 7:
        return ["Group", group_data]
    else:
        return ["Random", group_data]


"""COL_int, COL_double classification"""
def col_num_cls(values,data,s,mi,equipment_id,parameter_id):
    index_group, group_data = [], []

    for i in range(0,len(mi)+1):
        index_group.append([])

    if len(mi) == 0:
        if len(values) > 17:
            tmp = {}
            for i,value in enumerate(values):
                tmp.update({equipment_id[i]:value})
            group_data.append(json.dumps(tmp))
            return ["Constant", group_data]
        elif len(values) > 7:
            tmp = {}
            for i,value in enumerate(values):
                tmp.update({equipment_id[i]:value})
            group_data.append(json.dumps(tmp))
            return ["Group", group_data]
        else:
            for i, value in enumerate(values):
                group_data.append(json.dumps({equipment_id[i]:value}))
            return ["Random", group_data]
    elif len(mi) == 1:
        for i, value in enumerate(data):
            if value < s[mi[0]]:
                index_group[0].append(i)
            else:
                index_group[1].append(i)
    else:
        for i, value in enumerate(data):
            for j in range(0,len(mi)):
                if j ==0 :
                    if value < s[mi[j]]:
                        index_group[j].append(i)
                        break
                elif j == len(mi) -1:
                    if value > s[mi[j-1]] and value < s[mi[j]]:
                        index_group[j].append(i)
                        break
                    elif value > s[mi[j]]:
                        index_group[j+1].append(i)
                        break
                else:
                    if value > s[mi[j-1]] and value < s[mi[j]]:
                        index_group[j].append(i)
                        break
    print("index_group : ",index_group)
    cnt = 0
    for i, indexes in enumerate(index_group):
        if len(indexes) > 7:
            tmp = {}
            if len(indexes) > cnt:
                cnt = len(indexes)
            for j in  indexes:
                tmp.update({equipment_id[j]:values[j]})
            group_data.append(json.dumps(tmp))
        else:
            if len(indexes) > cnt:
                cnt = len(indexes)
            for j in indexes:
                group_data.append(json.dumps({equipment_id[j]:values[j]}))
    print("cnt : ",cnt)

    if cnt > 17:
        return ["Constant", group_data]
    elif cnt > 7:
        return ["Group", group_data]
    else:
        return ["Random", group_data]


"""text, bool classification"""
def text_cls(values, numRow, equipment_id, parameter_id):
    num_data = Counter(values).most_common()

    group_data = []

    for i in range(0,len(num_data)):
        indexes = [k for k,x in enumerate(values) if x== num_data[i][0]]
        if num_data[i][1] > 7:
            tmp = {}
            for index in indexes:
                tmp.update({equipment_id[index]:values[index]})
            group_data.append(json.dumps(tmp))
        else:
            for index in indexes:
                group_data.append(json.dumps({equipment_id[index]:values[index]}))

    mx = 0
    for element in num_data:
        if element[1] > mx:
            mx = element[1]

    if mx > 17:
        return ["Constant", group_data]
    elif mx > 7:
        return ["Group", group_data]
    else:
        return ["Random", group_data]


"""COL_text, COL_bool classification"""
def col_text_cls(values, numRow, equipment_id, parameter_id):

    list_cnt, group_data = [], []

    if len(values) == 1:###len of values ==1, Random
        for i in range(0,len(values)):
            group_data.append(json.dumps({equipment_id[i]:str(values[i])}))
        return  ["Random", group_data]

    if all(len(value) != len(values[0]) for value in values): #checking all parameter have same num of elements. if not Random
        for i in range(0,len(values)):
            group_data.append(json.dumps({equipment_id[i]:str(values[i])}))
        return ["Random", group_data]
    else:
        for i in range(0,len(values)):
            cnt = 0
            for j in range(0,len(values)):
                if values[i] == values[j]:
                    cnt += 1
            list_cnt.append(cnt)

        for i, cnt in enumerate(set(list_cnt)):
            if cnt > 7:
                tmp = {}
                for index, value in enumerate(values):
                    if value == values[list_cnt.index(cnt)]:
                        tmp.update({equipment_id[index]:values[index]})
                group_data.append(json.dumps(tmp))

        for i, cnt in enumerate(list_cnt):
            if cnt <= 7:
                group_data.append(json.dumps({equipment_id[i]:values[i]}))

    if max(list_cnt) > 17:
        return ["Constant", group_data]
    elif max(list_cnt) > 7:
        return ["Group", group_data]
    else:
        return ["Random", group_data]


def COL_KDE(values,numRow, equipment_id, parameter_id):

    if all(len(value) == len(values[0]) for value in values):
        pca = PCA(n_components = 1, whiten=False) #reduce dimensions to 1
        data = pca.fit_transform(values)

        if len(values) ==1: # if num of ele is 1, Random
            group_data = []
            for i in range(0,len(values)):
                group_data.append(json.dumps({int(i+1):str(values[i])}))
            return ["Random", group_data]
        else:
            grid = GridSearchCV(KernelDensity(), {'bandwidth' : np.linspace(0.008,1.5,30)},cv=len(values)) #range of bandwidth is 0.008~1.5
        grid.fit(data)
        kde = grid.best_estimator_

        dx = (max(data)-min(data))/len(data)
        left, right = min(data) - dx, max(data) + dx
        if round(float(left)) == round(float(right)) :
            left,right = np.mean(data)*0.5 - 0.5, np.mean(data)*1.5 + 0.5
        s = linspace(left,right)
        e = kde.score_samples(s.reshape(-1,1))
        mi, ma = argrelextrema(e, np.less)[0], argrelextrema(e, np.greater)[0]
        return col_num_cls(values,data,s,mi,equipment_id,parameter_id)
    else:
        group_data = []
        for i in range(0,len(values)):
            group_data.append(json.dumps({(i+1):values[i]}))
        return ["Random", group_data]


def KDE(values,numRow,equipment_id,parameter_id):
    values = array(values).reshape(-1,1)

    if len(values) ==1: # if num of ele is 1, Random
        group_data = []
        for i in range(0,len(values)):
            group_data.append(json.dumps({(i+1):values[i][0].item()}))
        return ["Random", group_data]
    else:
        grid = GridSearchCV(KernelDensity(), {'bandwidth': np.linspace(0.008, 1.5,30)}, cv=len(values))
    grid.fit(values)

    kde = KernelDensity(kernel='gaussian', bandwidth=grid.best_params_['bandwidth']).fit(values)
    dx = (max(values)-min(values))/len(values)
    left, right = min(values) - dx, max(values) + dx
    if(left==right):
        left,right = np.mean(values)*0.5 - 0.5, np.mean(values)*1.5 + 0.5
    s = linspace(left,right)
    e = kde.score_samples(s.reshape(-1,1))
    mi, ma = argrelextrema(e, np.less)[0], argrelextrema(e, np.greater)[0]
    return cls(values,s,mi,equipment_id,parameter_id)


def classification(values, numRow, value_type, equipment_id, parameter_id):

    if value_type == "DOUBLE" or value_type == "INT":
        return KDE(values, numRow, equipment_id, parameter_id)
    elif value_type == "TEXT" or value_type == "BOOL":
        return text_cls(values, numRow, equipment_id, parameter_id)
    elif value_type == "COL_DOUBLE" or value_type == "COL_INT":
        return COL_KDE(values, numRow, equipment_id, parameter_id)
    elif value_type == "COL_TEXT" or value_type == "COL_BOOL":
        return col_text_cls(values, numRow, equipment_id, parameter_id)

