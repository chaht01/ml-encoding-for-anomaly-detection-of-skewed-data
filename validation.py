from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from table_info import KCluster, Modes
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os
# from kmodes.util.dissim import ng_dissim

from util import windowed_query

DB_FILE = 'data2.db'


def get_dissim(X_s, lth_cluster, lth_centroid):
    """
    ng dissimilarity function 
    :param X: 
    :param lth_cluster: 
    :param lth_centroid: 
    :return: 
    """
    cl = len(lth_cluster)  # number of object included in lth_cluster
    lth_cluster = np.array(lth_cluster)
    dissim_matrix = [1]*len(lth_centroid)

    for i, attr in enumerate(lth_centroid):
        if attr == X_s[i]:
            unique, counts = np.unique(lth_cluster[:,i], return_counts=True)
            cljr_map = dict(zip(unique, counts))
            if X_s[i] not in cljr_map:
                continue
            else:
                dissim_matrix[i] -= cljr_map[X_s[i]]/cl
    return np.sum(dissim_matrix)


def silhouette(X, membship, centroids):
    """Get ith sample(X[i])'s silhouette coefficient value"""
    silhouette_result = [0]*len(X)

    clusters = []
    for memb in membship:
        m_map, = np.where(np.array(memb) == 1)
        cluster = []
        for idx in m_map:
            cluster.append(X[idx])
        clusters.append(cluster)

    for i, sample in enumerate(X):
        l, = np.where(np.array(membship)[:, i] == 1)

        if len(l) != 1:
            raise ValueError("Invalid membership value: it contains more than 2 value of 1 in specific column")
        cls_idx = l[0]
        lth_centroid = centroids[cls_idx]
        lth_cluster = clusters[cls_idx]
        # Get a(i) value
        a_i = get_dissim(sample, lth_cluster, lth_centroid)
        # Get b(i) value
        b_candidates = []
        for l_other, cluster in enumerate(clusters):
            if l_other != cls_idx:
                b_candidates.append(get_dissim(sample, clusters[l_other], centroids[l_other]))
        if len(b_candidates) == 0:
            raise ValueError("Cluster must be more than 1")
        b_i = min(b_candidates)
        silhouette_result[i] = (b_i - a_i)/max(a_i, b_i)
    return silhouette_result


def validation():
    engine = create_engine('sqlite:///%s' % DB_FILE)
    session = sessionmaker(bind=engine)()
    t0 = time.time()
    data = []
    output_dir = 'silhouette/'
    query = session.query(Modes)
    print('Starts Fetching data samples')
    for mode in windowed_query(query, Modes.id):
        data.append([int(i) for i in mode.modes.split(' ')])
    # Leave unique feature
    data = np.vstack({tuple(row) for row in data})
    data = np.array(np.transpose(data))
    query = session.query(KCluster)
    print('Ends Fetching data samples: ' + str(data.shape))
    for cluster in windowed_query(query, KCluster.id):
        cent_init = str(cluster.init)
        centroids = [[int(x) for x in centroid.split(' ')] for centroid in cluster.centroids.split(', ')]
        k = int(cluster.k)
        if k > 1:
            t1 = time.time()
            print('Starts "Silhouette" of ' + str(cent_init) + ' with k=' + str(k))
            k_result = [int(i) for i in cluster.clusters.split(' ')]
            memship = np.zeros((k, len(k_result)), dtype=np.uint8)
            for i, c in enumerate(k_result):
                memship[c][i] = 1
            fig = plt.figure(1)
            ax1 = fig.add_subplot(111)
            fig.set_size_inches(7, 7)
            ax1.set_xlim([-0.1, 0.5])
            ax1.set_ylim([0, len(data) + (k + 1) * 10])
            silhouette_values = silhouette(data, memship, centroids)
            silhouette_avg = np.average(silhouette_values)
            if not os.path.exists(output_dir+"result/"):
                os.makedirs(output_dir+"result/")
            with open(output_dir+"result/silhouette.txt", "a+") as f:
                f.writelines("Init: " + str(cent_init) + ", k: " + str(k) + ", silhouette: " + str(
                    silhouette_values) + ", avg: "+ str(silhouette_avg))
                print("Init: " + str(cent_init) + ", k: " + str(k) + ", silhouette: " + str(
                    silhouette_values) + ", avg: "+ str(silhouette_avg))
                f.close()
            y_lower = 10
            for ii in range(k):
                # Aggregate the silhouette scores for samples belonging to
                # cluster i, and sort them
                ith_cluster_silhouette_values = []
                for idx, item in enumerate(silhouette_values):
                    if k_result[idx] == ii:
                        ith_cluster_silhouette_values.append(item)

                ith_cluster_silhouette_values.sort()
                ith_cluster_silhouette_values = np.array(ith_cluster_silhouette_values)
                size_cluster_i = ith_cluster_silhouette_values.shape[0]
                y_upper = y_lower + size_cluster_i

                color = cm.spectral(float(ii) / k)
                ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                  0, ith_cluster_silhouette_values,
                                  facecolor=color, edgecolor=color, alpha=0.7)

                # Label the silhouette plots with their cluster numbers at the middle
                ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(ii) +"(" + str(size_cluster_i) + ")")

                # Compute the new y_lower for next plot
                y_lower = y_upper + 10  # 10 for the 0 samples
            ax1.set_title("The silhouette plot for KModes initialized by "+str(cent_init)+" and k="+str(k))
            ax1.set_xlabel("The silhouette coefficient values")
            ax1.set_ylabel("Cluster label")

            # The vertical line for average silhouette score of all the values
            ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

            ax1.set_yticks([])  # Clear the yaxis labels / ticks
            ax1.set_xticks([-0.1, 0, 0.5])
            if not os.path.exists(output_dir+"plot/"):
                os.makedirs(output_dir+"plot/")
            plt.savefig(output_dir+'plot/'+str(cent_init)+'_'+str(k)+'.png')
            plt.close()
            print('Ends "Silhouette" of '+str(cent_init)+' with k='+str(k)+': ', time.time() - t1, 'seconds')
    print('Total execute time for "Validation": ', time.time() - t0, 'seconds')
if __name__ == '__main__':
    validation()
