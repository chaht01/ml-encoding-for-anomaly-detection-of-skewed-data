from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from table_info import Modes, KCluster
import time
import numpy as np
import collections

from unique_feature import unique_feature
from util import windowed_query
DB_FILE = 'data.db'


def differ_centroid(init_k=2):
    engine = create_engine('sqlite:///%s' % DB_FILE)
    session = sessionmaker(bind=engine)()
    query = session.query(KCluster)
    t0 = time.time()
    _, keys = unique_feature()

    for cluster in windowed_query(query, KCluster.id):
        if int(cluster.k) == init_k:
            print("Initialized: "+str(cluster.init))
            centroids = [c_str.split(' ') for c_str in cluster.centroids.split(', ')]
            centroids = np.array(centroids)
            differ_map = []
            for i in range(centroids.shape[1]):
                if np.unique(centroids[:,i]).size == 1:
                    differ_map.append(0)
                else:
                    differ_map.append(1)

            print("Different parameter results:")
            print("For total "+str(_.shape[1])+" features, the number of difference: " + str(np.sum(differ_map)))

            for i, diff in enumerate(differ_map):
                print("At "+str(i)+"th param: ")
                print(keys[i])

if __name__ == '__main__':
    differ_centroid()

