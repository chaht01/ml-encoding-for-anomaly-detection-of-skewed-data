import sys, os
from kmodes.kmodes import KModes
from kmodes.util.dissim import ng_dissim, matching_dissim
import numpy as np
import collections
from table_info import Modes, KCluster
from unique_feature import unique_feature
from util import windowed_query
from sqlalchemy import create_engine, engine
from sqlalchemy.orm import sessionmaker
import matplotlib.pyplot as plt
import time

DB_FILE = 'data2.db'


def k_mode_cluster(n_clusters=3, n_iteration=10, dissim_as=ng_dissim, centroid_init='Cao'):
    engine = create_engine('sqlite:///%s' % DB_FILE)
    session = sessionmaker(bind=engine)()
    t0 = time.time()
    if not engine.dialect.has_table(engine, KCluster.__tablename__):  # If table don't exist, Create.
        KCluster.__table__.create(bind = engine)
    data, _ = unique_feature()

    km = KModes(n_clusters=n_clusters, init=centroid_init, n_init=n_iteration, verbose=1, cat_dissim=dissim_as)
    print("predict start")
    clusters = km.fit_predict(data)

    print(str(collections.Counter(clusters)))
    print(str(clusters))
    print(str(km.cluster_centroids_))
    print(str(km.cost_))
    session.add(KCluster(
        k=n_clusters,
        clusters=" ".join(str(x) for x in clusters),
        centroids=", ".join(" ".join(str(x) for x in cent) for cent in km.cluster_centroids_),
        cost=km.cost_,
        init=centroid_init))
    session.commit()
    print('Total execute time for "anomaly": ', time.time() - t0, 'seconds')
    return clusters, km.cluster_centroids_, km.cost_


def elbow_plot(k_set, path):
    engine = create_engine('sqlite:///%s' % DB_FILE)
    session = sessionmaker(bind=engine)()
    query = session.query(KCluster)

    cao_cost_list = [0]*len(k_set)
    huang_cost_list = [0]*len(k_set)
    for cluster in windowed_query(query, KCluster.id):
        if cluster.init == 'Cao':
            cao_cost_list[cluster.k-1] = cluster.cost
        elif cluster.init == 'Huang':
            huang_cost_list[cluster.k - 1] = cluster.cost

    # plt.plot(k_set, cao_cost_list)
    # plt.ylabel('cost')
    # plt.xlabel('# of clusters(k)')
    # plt.title('Cao')
    # plt.savefig(path+'cao.png')
    # plt.close()

    plt.plot(k_set, huang_cost_list)
    plt.ylabel('cost')
    plt.xlabel('# of clusters(k)')
    plt.title('Huang')
    plt.savefig(path+'huang.png')
    plt.close()

if __name__ == '__main__':

    orig_stdout = sys.stdout
    output_dir = 'anomaly/'
    if not os.path.exists(output_dir + "result/"):
        os.makedirs(output_dir + "result/")
    f = open(output_dir + "result/out.txt", 'w')
    sys.stdout = f
    k_set = range(1, 5)
    init_set = ['Huang']
    for centroid_init in init_set:
        for i in k_set:
            k_mode_cluster(n_clusters=i, centroid_init=centroid_init)

    sys.stdout = orig_stdout
    f.close()

    if not os.path.exists(output_dir + "plot/"):
        os.makedirs(output_dir + "plot/")
    elbow_plot(k_set, output_dir + "plot/")


