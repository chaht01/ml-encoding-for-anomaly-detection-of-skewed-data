import json
import re
from table_info import Classification, Modes
from util import windowed_query
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import ast, time

DB_FILE = 'data2.db'


def encode_modes():
    engine = create_engine('sqlite:///%s' % DB_FILE)
    if not engine.dialect.has_table(engine, Modes.__tablename__):  # If table don't exist, Create.
        Modes.__table__.create(bind = engine)

    session = sessionmaker(bind=engine)()
    query = session.query(Classification)
    numRow = 0
    t0 = time.time()

    for cls in windowed_query(query, Classification.id):
        test = re.compile(r"'({.*?})'\s*(?:,|\s*])")
        try:
            arr = [json.loads(x) for x in test.findall(cls.cls_cluster)]
        except ValueError:
            arr = [json.loads(json.dumps(ast.literal_eval(x))) for x in test.findall(cls.cls_cluster)] # Handle some parse error caused by escape character '\'

        arr.sort(key=lambda x: len(x), reverse=True)

        l = [None] * 5
        if not cls.cls_result == "Random":
            # Ignore "Random" parameter.
            numRow += 1
            if cls.cls_result == "Constant":
                # "Constant" parameter handles constant mode as 0 and others as 4, 5, ... and so on.
                diff = 4
                cnt  = 0
                for i in range(len(arr)):
                    for key in arr[i]:
                        try:
                            l[int(key) - 1] = 0 if i is 0 else diff
                            cnt = cnt + 1 if i is 0 else cnt
                            diff = diff + 1 if i is not 0 else diff
                        except ValueError:
                            print(cls.parameter_id, arr)
            else:
                # "Group" parameter handles group mode as 1, 2, 3 (labeled sort by group size) and other as 4, 5, ... and so on.
                diff = 1
                for i in range(len(arr)):
                    if len(arr[i]) < 8:
                        diff = max(diff, 3)
                    for key in arr[i]:
                        l[int(key) - 1] = i + diff

            modes = Modes(classification_id=cls.id, modes=imputing(l))
            session.add(modes)
            if numRow % 1000 == 0:
                session.commit()
    session.commit()
    print('Total execute time for "encode_mode": ', time.time() - t0, 'seconds')


def imputing(list_with_none):
    """ 
     Imputing
     Impute None value as 4,5,6,... (not in Group and Constant)
     If values except None are all equals each columns, we can impute None value as valid value.
     However, we don't make them that way because we thought it is not fair --- classified value that was not in group 'Constant' or 'Group'.
    """
    curr_panelty = max(3, max([i for i in list_with_none if i is not None])) + 1
    sanitized = []
    for elem in list_with_none:
        if elem is None:
            sanitized.append(curr_panelty)
            curr_panelty += 1
        else:
            sanitized.append(elem)

    return " ".join(str(x) for x in sanitized)


if __name__ == '__main__':
    encode_modes()
