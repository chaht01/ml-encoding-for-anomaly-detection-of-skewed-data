from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, Float, ARRAY
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()


class Equipment(Base):
    __tablename__ = 'tb_equipment'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return "<Equipment(id='%s', name='%s')>" % (self.id, self.name)


class Parameter(Base):
    __tablename__ = 'tb_parameter'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    value_type = Column(String)

    values = relationship("EquipmentParameter", back_populates="parameter")

    def __repr__(self):
        return "<Parameter(id='%s', name='%s', value_type='%s')>" % (self.id, self.name, self.value_type)


class EquipmentParameter(Base):
    __tablename__ = 'tb_equipment_parameter'
    id = Column(Integer, primary_key=True)
    equipment_id = Column(Integer, ForeignKey('tb_equipment.id'))
    parameter_id = Column(Integer, ForeignKey('tb_parameter.id'), index=True)
    value = Column(String)
    __table_args__ = (UniqueConstraint('equipment_id', 'parameter_id', name='u_equip_param'), )

    equipment = relationship("Equipment")
    parameter = relationship("Parameter", back_populates="values")

    def __repr__(self):
        return "<EquipmentParameter(id='%s', equipment='%s', parameter='%s', value='%s')>"\
               % (self.id, self.equipment.name, self.parameter.name, self.value)


class Classification(Base):
    __tablename__ = 'tb_classification'
    id = Column(Integer, primary_key=True)
    parameter_id = Column(Integer, ForeignKey('tb_parameter.id'), index=True)
    cls_result = Column(String)
    cls_cluster = Column(String)


class Modes(Base):
    __tablename__ = 'tb_modes'
    id = Column(Integer, primary_key=True)
    classification_id = Column(Integer, ForeignKey('tb_classification.id'), index=True)
    modes = Column(String)


class KCluster(Base):
    __tablename__ = 'tb_kcluster'
    id = Column(Integer, primary_key=True)
    k = Column(Integer)
    clusters = Column(String)
    centroids = Column(String)
    cost = Column(Float)
    init = Column(String)
