from table_info import Base, Equipment, Parameter, EquipmentParameter
from util import get_or_create
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import csv
import os
import time

DB_FILE = 'data2.db'
CSV_FILE = 'demo_Sample.csv'

if __name__ == '__main__':
    try:
        os.remove(DB_FILE)
    except OSError:
        pass
    engine = create_engine('sqlite:///%s' % DB_FILE)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    with open(CSV_FILE, 'r') as csvfile:
        reader = csv.reader(csvfile)
        numRow = 0
        t0 = time.time()
        for row in reader:
            numRow += 1
            if numRow < 3:
                continue
            parameter_name = '%s_%s_%s_%s' % (row[3], row[1], row[2], row[4])
            equipment_name = row[0]
            value_type = row[-1]
            value = ','.join(row[5:-1])
            equipment = get_or_create(session, Equipment, name=equipment_name)
            parameter = session.query(Parameter).filter_by(name=parameter_name).first()
            if parameter is None:
                parameter = Parameter(name=parameter_name, value_type=value_type)
                session.add(parameter)
            equipment_parameter = session.query(EquipmentParameter).filter_by(equipment=equipment, parameter=parameter).first()
            if equipment_parameter is None:
                equipment_parameter = EquipmentParameter(value=value, equipment=equipment, parameter=parameter)
                session.add(equipment_parameter)
            if numRow % 1000 == 0:
                print(numRow, time.time() - t0, equipment_parameter)
                session.flush()
    session.commit()
