import sys
from kmodes.kmodes import KModes
from kmodes.util.dissim import ng_dissim, matching_dissim
import numpy as np
import collections
from table_info import Modes, Cluster, KCluster
from util import windowed_query
from sqlalchemy import create_engine, engine
from sqlalchemy.orm import sessionmaker
from sklearn.feature_selection import SelectPercentile
from sklearn.feature_selection import chi2
import matplotlib.pyplot as plt
import time

DB_FILE = 'data.db'


def migrate():
    engine = create_engine('sqlite:///%s' % DB_FILE)
    if not engine.dialect.has_table(engine, KCluster.__tablename__):  # If table don't exist, Create.
        KCluster.__table__.create(bind = engine)
    session = sessionmaker(bind=engine)()
    query = session.query(Cluster)
    t0 = time.time()
    data = []

    for cluster in windowed_query(query, Cluster.id):
        if cluster.id<18:
            session.add(KCluster(k=cluster.k, clusters=cluster.clusters, centroids=cluster.centroids, cost=cluster.cost, init='Cao'))
        elif cluster.id <30:
            session.add(
                KCluster(k=cluster.k, clusters=cluster.clusters, centroids=cluster.centroids, cost=cluster.cost, init='Huang'))
        elif cluster.id ==30:
            session.add(
                KCluster(k=cluster.k, clusters=cluster.clusters, centroids=cluster.centroids, cost=cluster.cost, init='Huang'))
        else:
            session.add(
                KCluster(k=cluster.k, clusters=cluster.clusters, centroids=cluster.centroids, cost=cluster.cost, init='Cao'))
    session.commit()

if __name__ == '__main__':
    migrate()


