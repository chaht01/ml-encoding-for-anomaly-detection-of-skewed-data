# Requirements
 - python 3.6.3
 - sqlalchemy
```
$ pip install sqlalchemy

$ pip install scipy
$ pip install -U scikit-learn
$ pip install matplotlib
$ pip install numpy
$ pip install kmodes

```
 - or you can use requirements.txt
```
$ pip install -r requirements.txt
```

# Usage
 - generate data.db
```
$ python reset_table.py
```
 - read data.db and execute classification of parameters
```
$ python read.py
```
 - encode mode and make table
```
$ python encode_mode.py
```
 - cluster by k-mode (to run this, "kmode" package required)
```
$ python anomaly.py
```
