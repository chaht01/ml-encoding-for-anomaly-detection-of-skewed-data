from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from table_info import Modes
import time
import numpy as np
import collections
from util import windowed_query
DB_FILE = 'data2.db'


def unique_feature():
    engine = create_engine('sqlite:///%s' % DB_FILE)
    session = sessionmaker(bind=engine)()
    query = session.query(Modes)
    t0 = time.time()
    data = []

    for mode in windowed_query(query, Modes.id):
        data.append(mode.modes.split(' '))
    print("Shape of origin data: " + str(np.array(np.transpose(data)).shape))

    # Leave unique feature

    sets = []
    keys = {}
    for i, row in enumerate(data):
        if row not in sets:
            sets.append(row)
            keys[sets.index(row)] = []

        keys[sets.index(row)].append(int(i))

    data = sets
    param_map = collections.Counter(np.min(np.array(data).astype(np.int32), 1))
    data = np.array(np.transpose(data))
    print("Shape of unique featured data: " + str(data.shape))
    print("Constant: "+str(param_map[0])+", Group: "+str(param_map[1]))
    return data, keys


if __name__ == '__main__':
    unique_feature()


