from table_info import Parameter, Classification, Base
from util import windowed_query
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, joinedload
from classification import classification
from classification_test import classification_test

import time, sqlite3, sys

DB_FILE = 'data2.db'


def to_int(a):
    try:
        return int(a)
    except ValueError:
        a += '0'
        return int(a)


def to_float(a):
    try:
        return float(a)
    except ValueError:
        a += '0'
        return float(a)


def to_bool(a):
    if a == 'AAue':
        return True
    elif a == 'FalAe':
        return False
    return True


if __name__ == '__main__':
    engine = create_engine('sqlite:///%s' % DB_FILE)
    session = sessionmaker(bind=engine)()
    Base.metadata.create_all(engine)
    query = session.query(Parameter).options(joinedload(Parameter.values))
    numRow = 0
    t0 = time.time()

    for parameter in windowed_query(query, Parameter.id):
        numRow += 1
        values = map(lambda x: x.value, parameter.values)
        if parameter.value_type.startswith('COL_'):
            values = map(lambda str: str.split('_'), values)
            if parameter.value_type.endswith('INT'):
                values = map(lambda lst: list(map(to_int, lst)), values)
            elif parameter.value_type.endswith('DOUBLE'):
                values = map(lambda lst: list(map(to_float, lst)), values)
            elif parameter.value_type.endswith('BOOL'):
                values = map(lambda lst: list(map(to_bool, lst)), values)
        else:
            if parameter.value_type.endswith('INT'):
                values = map(to_int, values)
            elif parameter.value_type.endswith('DOUBLE'):
                values = map(to_float, values)
            elif parameter.value_type.endswith('BOOL'):
                values = map(to_bool, values)
        values = list(values)

        #begin classification
        equipment_id = []
        for i in parameter.values:
            equipment_id.append(i.equipment_id)

        print(numRow, parameter.id, parameter.value_type, values)
        if "test" in sys.argv:
            result = classification_test(values, numRow, parameter.value_type, equipment_id, parameter.id)
        else:
            result = classification(values, numRow, parameter.value_type, equipment_id, parameter.id)

        cls = Classification(parameter_id=parameter.id, cls_result=result[0], cls_cluster=str(result[1]))
        session.add(cls)
        if numRow % 1000 == 1:
            session.commit()

    session.commit()
    print('Total time:', time.time() - t0, 'seconds')
